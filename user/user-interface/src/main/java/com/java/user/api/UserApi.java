package com.java.user.api;

import com.java.user.entity.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author jl
 * @description
 * @date 2019-11-05 15:58
 */
public interface UserApi {

    @GetMapping("/query")
    User queryBy(@RequestParam("username") String username, @RequestParam("password") String password);
}
