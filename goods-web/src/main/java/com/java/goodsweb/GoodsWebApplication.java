package com.java.goodsweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author jl
 * @description
 * @date 2019-10-31 11:02
 */
@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
public class GoodsWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(GoodsWebApplication.class,args);
    }
}
