package com.java.common.utils;

/**
 * @author jl
 * @description
 * @date 2019-10-12 9:53
 */
public class QueryParams {
    /**
     * 查询关键字
     */
    private String key;
    /**
     * 当前页
     */
    private Integer page;
    /**
     * 每页显示记录数
     */
    private Integer rows;
    /**
     * 排序字段
     */
    private String sortBy;
    /**
     * 是否降序
     */
    private Boolean desc;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public Boolean getDesc() {
        return desc;
    }

    public void setDesc(Boolean desc) {
        this.desc = desc;
    }

    public QueryParams(String key, Integer page, Integer rows, String sortBy, Boolean desc) {
        this.key = key;
        this.page = page;
        this.rows = rows;
        this.sortBy = sortBy;
        this.desc = desc;
    }
}
