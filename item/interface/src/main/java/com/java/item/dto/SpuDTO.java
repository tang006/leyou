package com.java.item.dto;

import com.java.item.entity.Sku;
import com.java.item.entity.Spu;
import com.java.item.entity.SpuDetail;
import lombok.Data;

import java.util.List;

/**
 * @author jl
 * @description
 * @date 2019-10-16 17:03
 */
@Data
public class SpuDTO extends Spu {

    private String cname;
    private String bname;

    private SpuDetail spuDetail;
    private List<Sku> skus;

}
