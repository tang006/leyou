package com.java.item.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 商品分类和品牌的中间表，两者是多对多关系
 * 
 * @author jiangli
 * @email 31346337@qq.com
 * @date 2019-10-11 11:44:02
 */
@Data
@TableName("tb_category_brand")
@AllArgsConstructor
@NoArgsConstructor
public class CategoryBrand implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 商品类目id
	 */
	@TableId(type= IdType.INPUT)
	private Long categoryId;
	/**
	 * 品牌id
	 */
	private Long brandId;

}
