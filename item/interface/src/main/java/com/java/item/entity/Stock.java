package com.java.item.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 库存表，代表库存，秒杀库存等信息
 * 
 * @author jiangli
 * @email 31346337@qq.com
 * @date 2019-10-16 11:30:52
 */
@Data
@TableName("tb_stock")
public class Stock implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 库存对应的商品sku id
	 */
	@TableId(type = IdType.INPUT)
	private Long skuId;
	/**
	 * 可秒杀库存
	 */
	private Integer seckillStock;
	/**
	 * 秒杀总数量
	 */
	private Integer seckillTotal;
	/**
	 * 库存数量
	 */
	private Integer stock;

}
