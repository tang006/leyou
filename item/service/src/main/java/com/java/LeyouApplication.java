package com.java;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author jl
 * @description
 * @date 2019-10-10 9:26
 */
@SpringBootApplication
@EnableDiscoveryClient
public class LeyouApplication {

    public static void main(String[] args) {
        SpringApplication.run(LeyouApplication.class,args);
    }
}
