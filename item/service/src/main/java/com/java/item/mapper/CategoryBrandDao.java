package com.java.item.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java.item.entity.CategoryBrand;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品分类和品牌的中间表，两者是多对多关系
 * 
 * @author jiangli
 * @email 31346337@qq.com
 * @date 2019-10-11 11:44:02
 */
@Mapper
public interface CategoryBrandDao extends BaseMapper<CategoryBrand> {
	
}
