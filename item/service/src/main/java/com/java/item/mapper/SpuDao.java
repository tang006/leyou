package com.java.item.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java.item.entity.Spu;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu表，该表描述的是一个抽象性的商品，比如 iphone8
 * 
 * @author jiangli
 * @email 31346337@qq.com
 * @date 2019-10-16 11:30:52
 */
@Mapper
public interface SpuDao extends BaseMapper<Spu> {
	
}
