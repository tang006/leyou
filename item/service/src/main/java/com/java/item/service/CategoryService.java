package com.java.item.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.java.item.entity.Category;

import java.util.List;

/**
 * 商品类目表，类目和商品(spu)是一对多关系，类目与品牌是多对多关系
 *
 * @author jiangli
 * @email 31346337@qq.com
 * @date 2019-10-11 11:44:02
 */
public interface CategoryService extends IService<Category> {

    /**
     * 根据父id查询子节点
     * @param pid
     * @return
     */
    List<Category> queryCategoryById(Long pid);

    void saveNode(Category category);

    void deleteNode(Long id);

    List<Category> queryCategoryByBid(Long bid);

    List<String> queryCategoryNamesByIds(List<Long> ids);
}

