package com.java.item.service;

import com.java.item.entity.SpecGroup;
import com.java.item.entity.SpecParam;

import java.util.List;

/**
 * @author jl
 * @description
 * @date 2019-10-16 14:24
 */
public interface SpecificationService {
    List<SpecGroup> queryGroupsByCid(Long cid);

    List<SpecParam> queryParams(Long gid,Long cid,Boolean generic,Boolean searching);
}
