package com.java.item.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.item.entity.CategoryBrand;
import com.java.item.mapper.CategoryBrandDao;
import com.java.item.service.CategoryBrandService;
import org.springframework.stereotype.Service;


@Service("categoryBrandService")
public class CategoryBrandServiceImpl extends ServiceImpl<CategoryBrandDao, CategoryBrand> implements CategoryBrandService {


}