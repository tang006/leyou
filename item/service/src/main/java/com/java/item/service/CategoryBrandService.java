package com.java.item.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.java.item.entity.CategoryBrand;

/**
 * 商品分类和品牌的中间表，两者是多对多关系
 *
 * @author jiangli
 * @email 31346337@qq.com
 * @date 2019-10-11 11:44:02
 */
public interface CategoryBrandService extends IService<CategoryBrand> {

}

