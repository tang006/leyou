package com.java.item.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.java.common.utils.PageResult;
import com.java.common.utils.QueryParams;
import com.java.item.entity.Brand;

import java.util.List;

/**
 * 品牌表，一个品牌下有多个商品（spu），一对多关系
 *
 * @author jiangli
 * @email 31346337@qq.com
 * @date 2019-10-11 11:44:02
 */
public interface BrandService extends IService<Brand> {

    PageResult<Brand> queryBrandsByPage(QueryParams queryParams);

    /**
     * 新增品牌
     */
    void saveBrand(Brand brand, List<Long> cids);
    /**
     * 根据分类id查询品牌列表
     */
    List<Brand> queryByCid(Long cid);

    void updateBrand(Brand brand, List<Long> cids);

    void deleteBrand(Long bid);
}

