package com.java.item.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.java.item.entity.SpecGroup;
import com.java.item.entity.SpecParam;
import com.java.item.mapper.SpecGroupDao;
import com.java.item.mapper.SpecParamDao;
import com.java.item.service.SpecificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author jl
 * @description
 * @date 2019-10-16 14:24
 */
@Service
public class SpecificationServiceImpl implements SpecificationService {
    @Autowired
    private SpecGroupDao specGroupDao;
    @Autowired
    private SpecParamDao specParamDao;

    @Override
    public List<SpecGroup> queryGroupsByCid(Long cid) {
        List<SpecGroup> specGroups = specGroupDao.selectList(new LambdaQueryWrapper<SpecGroup>().eq(SpecGroup::getCid, cid));
        specGroups.forEach(specGroup -> {
            List<SpecParam> specParams = this.queryParams(specGroup.getId(), null, null, null);
            specGroup.setParams(specParams);
        });
        return specGroups;
    }

    @Override
    public List<SpecParam> queryParams(Long gid, Long cid, Boolean generic, Boolean searching) {
        return specParamDao.selectList(new LambdaQueryWrapper<SpecParam>()
                .eq(gid != null, SpecParam::getGroupId, gid)
                .eq(cid != null, SpecParam::getCid, cid)
                .eq(generic != null, SpecParam::getGeneric, generic)
                .eq(searching != null, SpecParam::getSearching, searching));
    }
}
