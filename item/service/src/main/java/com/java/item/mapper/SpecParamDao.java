package com.java.item.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java.item.entity.SpecParam;
import org.apache.ibatis.annotations.Mapper;

/**
 * 规格参数组下的参数名
 * 
 * @author jiangli
 * @email 31346337@qq.com
 * @date 2019-10-16 11:30:52
 */
@Mapper
public interface SpecParamDao extends BaseMapper<SpecParam> {
	
}
