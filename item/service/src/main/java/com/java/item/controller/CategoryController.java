package com.java.item.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.java.item.entity.Category;
import com.java.item.service.CategoryService;


/**
 * 商品类目表，类目和商品(spu)是一对多关系，类目与品牌是多对多关系
 *
 * @author jiangli
 * @email 31346337@qq.com
 * @date 2019-10-11 11:44:02
 */
@RestController
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    /**
     * 根据父id查询子节点
     */
    @GetMapping("/list")
    public ResponseEntity<List<Category>> list(@RequestParam(value = "pid", defaultValue = "0") Long pid) {
        if (pid < 0) pid = 0L;
        List<Category> categories = categoryService.queryCategoryById(pid);
        return ResponseEntity.ok(categories);
    }

    /**
     * 新增节点
     */
    @PostMapping("/node")
    public ResponseEntity<Category> save(@RequestBody Category category) {
        categoryService.saveNode(category);
        return ResponseEntity.ok(category);
    }

    /**
     * 删除节点
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable String id) {
        categoryService.deleteNode(Long.valueOf(id));
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    /**
     * 根据品牌信息查询商品分类
     */
    @GetMapping("/bid/{bid}")
    public ResponseEntity<List<Category>> queryCategoryByBid(@PathVariable("bid") Long bid) {
        List<Category> categories = categoryService.queryCategoryByBid(bid);
        return ResponseEntity.ok(categories);
    }

    /**
     * 根据分类ids查询分类名称集合
     */
    @GetMapping
    public ResponseEntity<List<String>> queryCategoryNamesByIds(@RequestParam("ids") List<Long> ids) {
        List<String> names = categoryService.queryCategoryNamesByIds(ids);
        return ResponseEntity.ok(names);
    }

}
