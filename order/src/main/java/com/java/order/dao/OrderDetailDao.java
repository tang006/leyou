package com.java.order.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java.order.entity.OrderDetail;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单详情表
 * 
 * @author jiangli
 * @email 31346337@qq.com
 * @date 2019-11-08 14:20:23
 */
@Mapper
public interface OrderDetailDao extends BaseMapper<OrderDetail> {
	
}
