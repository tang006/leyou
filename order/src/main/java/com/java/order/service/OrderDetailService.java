package com.java.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.java.order.entity.OrderDetail;

/**
 * 订单详情表
 *
 * @author jiangli
 * @email 31346337@qq.com
 * @date 2019-11-08 14:20:23
 */
public interface OrderDetailService extends IService<OrderDetail> {

}

