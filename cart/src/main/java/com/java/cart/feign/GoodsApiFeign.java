package com.java.cart.feign;

import com.java.item.api.GoodsApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author jl
 * @description
 * @date 2019-11-07 15:56
 */
@FeignClient("item-service")
public interface GoodsApiFeign extends GoodsApi {
}
