package com.java.search.pojo;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author jl
 * @description
 * @date 2019-10-25 15:28
 */
@Document(indexName = "goods",type = "docs",shards = 1,replicas = 0)
@Data
public class Goods {
    @Id
    private Long id;//spuId
    /**
     * String类型要加@Field,其它类型elasticsearch会自动判断
     */
    @Field(type = FieldType.Text,analyzer = "ik_max_word")
    private String all;//用来进行全文检索的字段,包含标题、分类、品牌等
    @Field(type = FieldType.Keyword,index = false)
    private String  subTitle;//卖点
    private Long brandId;//品牌id
    private Long cid1;//1级分类id
    private Long cid2;//2级分类id
    private Long cid3;//3级分类id
    private Date createTime;//创建时间
    private List<Long> price;//价格数组,是所有sku价格的集合,方便根据价格进行筛选过滤
    @Field(type = FieldType.Keyword,index = false)
    private String skus;//List<sku>信息的json结构
    private Map<String,Object> specs;//可搜索的参数规格,key是参数名,值是参数值


}
