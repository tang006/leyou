package com.java.search.repository;

import com.java.search.pojo.Goods;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author jl
 * @description
 * @date 2019-10-28 10:28
 */
public interface GoodsRepository extends ElasticsearchRepository<Goods,Long> {
}
