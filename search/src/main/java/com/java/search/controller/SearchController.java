package com.java.search.controller;

import com.java.search.pojo.SearchRequest;
import com.java.search.pojo.SearchResult;
import com.java.search.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author jl
 * @description
 * @date 2019-10-28 15:03
 */
@Controller
public class SearchController {

    @Autowired
    private SearchService searchService;

    @PostMapping("/page")
    public ResponseEntity<SearchResult> search(@RequestBody SearchRequest request) {
        SearchResult goodsPageResult = searchService.search(request);
        if (goodsPageResult==null || CollectionUtils.isEmpty(goodsPageResult.getItems())) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(goodsPageResult);
    }
}
