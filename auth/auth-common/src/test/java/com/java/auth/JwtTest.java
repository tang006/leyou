package com.java.auth;

import com.java.auth.entity.UserInfo;
import com.java.auth.utils.JwtUtils;
import com.java.auth.utils.RsaUtils;
import org.junit.Before;
import org.junit.Test;

import java.security.PrivateKey;
import java.security.PublicKey;

public class JwtTest {
    private static String PUBLIC_KEY_PATH="rsa.pub";
    private static String PRIVATE_KEY_PATH="rsa.pri";
    private PublicKey publicKey;
    private PrivateKey privateKey;

    /**
     * 生成公钥和私钥
     */
    @Test
    public void testRsa() throws Exception {
        //根据密文"c8dbefeb8b3843dc8b70c610ef572858"，生存rsa公钥和私钥,并写入指定文件
        RsaUtils.generateKey(PUBLIC_KEY_PATH, PRIVATE_KEY_PATH, "c8dbefeb8b3843dc8b70c610ef572858");
    }

    /**
     * 读取公钥和私钥
     */
    @Before
    public void testGetRsa() throws Exception {
        //从文件中读取公钥
        this.publicKey = RsaUtils.getPublicKey(PUBLIC_KEY_PATH);
        //从文件中读取私钥
        this.privateKey = RsaUtils.getPrivateKey(PRIVATE_KEY_PATH);
    }

    /**
     * 生成token
     */
    @Test
    public void testGenerateToken() throws Exception {
        // 生成token
        String token = JwtUtils.generateToken(new UserInfo(1L, "admin"), privateKey, 5);
        System.out.println("token = " + token);
    }

    /**
     * 解析token
     */
    @Test
    public void testParseToken() throws Exception {
        //上边生成的token
        String token = "eyJhbGciOiJSUzI1NiJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJhZG1pbiIsImV4cCI6MTU3MjkzODkxM30.dPiz9aJl2pG5AnLRnGgNsty4kIKPwQ79bYV2-Th3tsiOiiHe4LnTpIPLOHfGtN-VecXsCesq4CIxk6eREKFMSuEamdJ22bRym2olI9RsURa-ARsQpnyjP0hRwZfW7fqC-s1JVWpCaYddNubs3r5du7tv5RR-6KC9LWCHjyaV0eU";

        // 解析token
        UserInfo user = JwtUtils.getInfoFromToken(token, publicKey);
        System.out.println("id: " + user.getId());
        System.out.println("userName: " + user.getUsername());
    }
}