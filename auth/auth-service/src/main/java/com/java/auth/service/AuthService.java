package com.java.auth.service;

import com.java.auth.config.JwtProperties;
import com.java.auth.entity.UserInfo;
import com.java.auth.feign.UserApiFeign;
import com.java.auth.utils.JwtUtils;
import com.java.user.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author jl
 * @description
 * @date 2019-11-05 15:47
 */
@Service
public class AuthService {
    @Autowired
    private UserApiFeign userApiFeign;
    @Autowired
    private JwtProperties jwtProperties;

    public String accredit(String username, String password) {
        User user = userApiFeign.queryBy(username, password);
        if (user == null) {
            return null;
        }
        //生成jwtToken
        UserInfo userInfo = new UserInfo();
        userInfo.setId(user.getId());
        userInfo.setUsername(user.getUsername());
        try {
            return JwtUtils.generateToken(userInfo,jwtProperties.getPrivateKey(),jwtProperties.getExpire());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
